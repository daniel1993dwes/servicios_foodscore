const express = require("express");
const router = express.Router();
const User = require("./../models/user");
const fs = require("fs");
const constant = require('./../constants/constants');
const passport = require("passport");
const AuthService = require('../services/auth-services');
const Token = require('../token/token');

let time = constant.timeStamp;

router.post("/login", (req, res) => {
  User.validarUsuario({ email: req.body.email, password: req.body.password })
    .then(resultado => {
      if (resultado) res.send({ ok: true, accessToken: resultado });
      else res.status(400).send({ ok: false, mensajeError: "Usuario incorrecto" });
    })
    .catch(error => {
      res.status(400).send({ ok: false, mensajeError: "Usuario incorrecto", mensajeConcreto: error });
    });
});

router.post("/register", (req, res) => {
  let usuario = new User(req.body);
  usuario.AVATAR = './images/users/' + time + '.jpg';
  
  usuario.nuevoUsuario().then(result => {
    let archivo = Buffer.from(req.body.avatar, 'base64');
    fs.writeFile('./images/users/' + time + '.jpg', archivo, 'base64', (err) => {
      if (err) throw err;
      console.log('Archivo de usuario subido con exito');
    });
      res.send(result);
    }).catch(error => {
      res.status(400).send({
        error: "Bad Request",
        message: error
      });
    });
});

router.get("/validate", passport.authenticate("jwt", { session: false }),
 (req, res) => {
   if (req.user) {
    res.status(200).send({ ok: true });
   } else {
    res.status(401).send("Unauthorized");
   }
  
});

router.post("/google", async (req, res) => {
  let token = req.body.token;
  AuthService.getProfileGoogle(token).then(result => {
    res.status(200).send({ token: result })
  }).catch(error =>{
    res.status(401).send({error: true, mensajeError: 'Unauthorized'});
  });
});

router.post("/facebook", async (req, res) => {
  let token = req.body.token;
  AuthService.getProfileFacebook(token).then(result => {
    res.status(200).send({ token: result })
  }).catch(error =>{
    res.status(401).send({error: true, mensajeError: 'Unauthorized'});
  });
});

module.exports = router;
