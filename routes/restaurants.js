const express = require('express');
const router = express.Router();
const constant = require('./../constants/constants');
const Restaurante = require('../models/restaurant');
const Usuario = require('../models/user');
const Token = require('../token/token');
const passport = require('passport');
const fs = require('fs');

let time = constant.timeStamp;

router.get("",
passport.authenticate('jwt', { session: false }),
(req, res) => {

    Restaurante.getAllRestaurants(req.user.id).then(result => {
        res.status(200).send({ Restaurants: result, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    })
});

router.get("/mine",
passport.authenticate('jwt', { session: false }),
(req, res) => {

    Restaurante.getMyRestaurants(req.user.id).then(result => {
        res.status(200).send({ Restaurants: result, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    })
});

router.get("/user/:id",
passport.authenticate('jwt', { session: false }),
(req, res) => {

    Restaurante.getUserRestaurants(req.params.id, req.user.id).then(result => {
        res.status(200).send({ Restaurants: result, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(404).send({error: true, mensajeError: 'Not Found'});
    })
});

router.get("/:id",
passport.authenticate('jwt', { session: false }),
(req, res) => {

    Restaurante.getRestaurantId(req.params.id, req.user.id).then(result => {
        res.status(200).send({ Restaurants: result, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(404).send({error: true, mensajeError: 'Not Found'});
    })
});

router.post("",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restaurante = new Restaurante(req.body);
    restaurante.IMAGE = './images/restaurants/' + time + '.jpg';

    restaurante.addRestaurant(req.user.id).then(async result => {
        let archivo = Buffer.from(req.body.image, 'base64');
        await fs.writeFile('./images/restaurants/' + time + '.jpg', archivo, 'base64', (err) => {
        if (err) throw err;
            console.log('Archivo de restaurante subido con exito');
        });

        res.status(200).send({ result: result, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    })
});

router.put("/:id",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restaurante = new Restaurante(req.body);
    let restId = req.params.id;
    restaurante.IMAGE = './images/restaurants/' + time + '.jpg';

    restaurante.putRestaurant(restId, req.user.id).then(async result => {
        let archivo = Buffer.from(req.body.image, 'base64');
        await fs.writeFile('./images/restaurants/' + time + '.jpg', archivo, 'base64', (err) => {
        if (err) throw err;
            console.log('Archivo de restaurante subido con exito');
        });

        res.status(200).send({ result: result, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(404).send({error: true, mensajeError: 'Not Found'});
    })
});

router.delete("/:id",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restId = req.params.id;

    Restaurante.deleteRestaurant(restId, req.user.id).then(result => {
        res.status(200).send({ result: restId, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(404).send({error: true, mensajeError: 'Not Found'});
    })
});

router.get("/:id/comments",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restId = req.params.id;

    Restaurante.getRestaurantComments(restId).then(result => {
        let comentarios = [];
        result.forEach(comment => {
            let comentario = {
                id: comment.id,
                stars: comment.stars,
                restaurantId: comment.restaurantId,
                userId: comment.userId,
                text: comment.text,
                date: comment.date,
                user: {
                    name: comment.name,
                    avatar: comment.avatar,
                    email: comment.email,
                    lat: comment.lat,
                    lng: comment.lng,
                }
            };
            comentarios.push(comentario);
        })
        res.status(200).send({ comments: comentarios, accessToken: Token.generarToken(req.user.id) });
    }).catch(error => {
        res.status(404).send({error: true, mensajeError: 'Not Found'});
    })
});

router.post("/:id/comments",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restId = req.params.id;

    Restaurante.getRestaurantComments(restId).then(result => {
        result.forEach(comment => {
            if (comment.restaurantId == restId) {
                if (comment.userId = req.user.id) {
                    res.status(400).send({error: true, mensajeError: 'Only one comment allowed per restaurant and user'});
                }
            }
        })
        let coment = {
            stars: req.body.stars,
            text: req.body.text,
            restaurantId: restId,
            userId: req.user.id
        }
        Restaurante.addComment(coment).then(resp => {
            Restaurante.getCommentUser(resp.insertId, req.user.id).then(comment => {
                let comentario = {
                    id: comment.id,
                    stars: comment.stars,
                    restaurantId: comment.restaurantId,
                    userId: comment.userId,
                    text: comment.text,
                    date: comment.date,
                    user: {
                        name: comment.name,
                        avatar: comment.avatar,
                        email: comment.email,
                        lat: comment.lat,
                        lng: comment.lng,
                    }
                };
                res.status(200).send({ comment: comentario, accessToken: Token.generarToken(req.user.id) });
            })
        }).catch(error => {
            res.status(404).send({error: true, mensajeError: 'Not Found'});
        });
    }).catch(error => {
        res.status(404).send({error: true, mensajeError: 'Not Found'});
    });
});

module.exports = router;