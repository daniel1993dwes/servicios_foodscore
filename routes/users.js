const express = require('express');
const router = express.Router();
const passport = require('passport');
const Usuario = require('../models/user');
const Token = require('../token/token');
const constant = require('./../constants/constants');
const fs = require('fs');

let time = constant.timeStamp;

router.get("/me",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    Usuario.getUser(req.user.id).then(result => {
        res.status(200).send({user: result, token: Token.generarToken(req.user.id)});
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    });
});

router.get("/:id",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    let userId = req.params.id;
    Usuario.getUser(userId).then(result => {
        res.status(200).send({user: result, token: Token.generarToken(req.user.id)});
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    });
});

router.put("/me",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    Usuario.getUser(req.user.id).then(resp => {
        let usuario = new Usuario(resp);
        usuario.EMAIL = req.body.email;
        usuario.NAME = req.body.name;
        usuario.updateUsuario(req.user.id).then(result => {
            res.status(200).send({ok: true, token: Token.generarToken(req.user.id)});
        }).catch(error => {
            res.status(400).send({error: true, mensajeError: 'Bad Request'});
        });
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    });
});

router.put("/me/avatar",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    Usuario.getUser(req.user.id).then(resp => {
        let usuario = new Usuario(resp);
        usuario.AVATAR = './images/users/' + time + '.jpg';
        usuario.updateUsuario(req.user.id).then(result => {
            let archivo = Buffer.from(req.body.avatar, 'base64');
            fs.writeFile('./images/users/' + time + '.jpg', archivo, 'base64', (err) => {
            if (err) throw err;
                console.log('Archivo de restaurante subido con exito');
            });
            res.status(200).send({avatar: '/images/users/' + time + '.jpg', token: Token.generarToken(req.user.id)});
        }).catch(error => {
            res.status(400).send({error: true, mensajeError: 'Bad Request'});
        });
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    })
});

router.put("/me/password",
passport.authenticate('jwt', { session: false }),
(req, res) => {
    Usuario.getUser(req.user.id).then(resp => {
        let usuario = new Usuario(resp);
        usuario.PASSWORD = req.body.password;

        usuario.updateUsuario(req.user.id).then(result => {
            res.status(200).send({ok: true, token: Token.generarToken(req.user.id)});
        }).catch(error => {
            res.status(400).send({error: true, mensajeError: 'Bad Request'});
        });
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    });
});

module.exports = router;