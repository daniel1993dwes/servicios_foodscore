const conexion = require("../config/bdconfig");
const md5 = require("md5");
const jwt = require("jsonwebtoken");
const constant = require("../constants/constants");
const secreto = constant.secreto;

module.exports = class Token {
  static generarToken(id) {
    return jwt.sign({ id: id }, secreto, { expiresIn: "3 hours" });
  };

  static validarToken(token) {
    try {
      let resultado = jwt.verify(token, secreto);
      return resultado;
    } catch (e) {}
  }
};
