const conexion = require('../config/bdconfig');

module.exports = class Restaurant {
    constructor(restauranteJSON) {
        this.ID = restauranteJSON.id;
        this.NAME = restauranteJSON.name;
        this.DESCRIPTION = restauranteJSON.description;
        this.DAYSOPEN = restauranteJSON.daysOpen;
        this.PHONE = restauranteJSON.phone;
        this.IMAGE = restauranteJSON.image;
        this.CUISINE = restauranteJSON.cuisine;
        this.CREATORID = restauranteJSON.creatorId;
        this.STARS = restauranteJSON.stars || 0;
        this.LAT = restauranteJSON.lat || 0;
        this.LNG = restauranteJSON.lng || 0;
        this.ADDRESS = restauranteJSON.address;
        this.CREATORID = restauranteJSON.creatorId;
        this.DISTANCE = restauranteJSON.DISTANCE;
        this.MINE = restauranteJSON.mine;
    }

    static getAllRestaurants(id) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT RESTAURANT.*, USER.*, haversine(USER.LAT, USER.LNG, RESTAURANT.LAT, RESTAURANT.LNG) AS DISTANCE FROM RESTAURANT, USER WHERE USER.ID = ? AND RESTAURANT.ID = RESTAURANT.ID",
            [id],
            (error, resultado, campos) =>{
                if (error) return reject(error);
                else return resolve(resultado.map(restaurante => {
                    restaurante.cuisine = restaurante.cuisine.split(',');
                    restaurante.daysOpen = restaurante.daysOpen.split(',');
                    if(restaurante.creatorId == id) {
                        restaurante.mine = true;
                    } else {
                        restaurante.mine = false;
                    }
                    return new Restaurant(restaurante);
                }));
            })
        })
    }

    static getMyRestaurants(id) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT RESTAURANT.*, USER.LAT, USER.LNG, USER.ID AS IDENT, haversine(USER.LAT, USER.LNG, RESTAURANT.LAT, RESTAURANT.LNG) AS DISTANCE FROM RESTAURANT, USER WHERE USER.ID = ? AND RESTAURANT.ID = RESTAURANT.ID AND CREATORID = ? ",
            [id, id],
            (error, resultado, campos) =>{
                resultado
                if (error) return reject(error);
                else return resolve(resultado.map(restaurante => {
                    if(restaurante.creatorId == id) {
                        restaurante.mine = true;
                    } else {
                        restaurante.mine = false;
                    }
                    restaurante.cuisine = restaurante.cuisine.split(',');
                    restaurante.daysOpen = restaurante.daysOpen.split(',');
                    return new Restaurant(restaurante);
                }));
            })
        })
    }

    static getUserRestaurants(userId, id) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT RESTAURANT.*, USER.LAT, USER.LNG, USER.ID AS IDENT, haversine(USER.LAT, USER.LNG, RESTAURANT.LAT, RESTAURANT.LNG) AS DISTANCE FROM RESTAURANT, USER WHERE USER.ID = ? AND RESTAURANT.ID = RESTAURANT.ID AND RESTAURANT.CREATORID = ? ",
            [id, userId],
            (error, resultado, campos) =>{
                resultado
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado.map(restaurante => {
                    if(restaurante.creatorId == id) {
                        restaurante.mine = true;
                    } else {
                        restaurante.mine = false;
                    }
                    restaurante.cuisine = restaurante.cuisine.split(',');
                    restaurante.daysOpen = restaurante.daysOpen.split(',');
                    return new Restaurant(restaurante);
                }));
            })
        })
    }

    static getRestaurantId(restId, id) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT RESTAURANT.*, USER.LAT, USER.LNG, USER.ID AS IDENT, haversine(USER.LAT, USER.LNG, RESTAURANT.LAT, RESTAURANT.LNG) AS DISTANCE FROM RESTAURANT, USER WHERE USER.ID = ? AND RESTAURANT.ID = ? ",
            [id, restId],
            (error, resultado, campos) =>{
                resultado
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado.map(restaurante => {
                    if(restaurante.creatorId == id) {
                        restaurante.mine = true;
                    } else {
                        restaurante.mine = false;
                    }
                    restaurante.cuisine = restaurante.cuisine.split(',');
                    restaurante.daysOpen = restaurante.daysOpen.split(',');
                    return new Restaurant(restaurante);
                }));
            })
        })
    }

    static deleteRestaurant(restId, id) {
        return new Promise((resolve, reject) =>{
            conexion.query("DELETE FROM RESTAURANT WHERE ID = ? AND CREATORID = ? ",
            [restId, id],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getRestaurantComments(restId) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT COMMENT.*, USER.* FROM COMMENT, USER WHERE COMMENT.RESTAURANTID = ? AND USER.ID = COMMENT.USERID",
            [restId],
            (error, resultado, campos) =>{
                console.log('llego a la select');
                console.log(resultado);
                if (error) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static addComment(comment) {
        console.log(comment);
        return new Promise((resolve, reject) =>{
            conexion.query("INSERT INTO COMMENT SET ? ",
            [comment],
            (error, resultado, campos) =>{
                console.log('llego a la select');
                console.log(resultado);
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getCommentUser(id, userId) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT COMMENT.*, USER.* FROM COMMENT, USER WHERE COMMENT.ID = ? AND USER.ID = ? ",
            [id, userId],
            (error, resultado, campos) =>{
                if (error) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    addRestaurant(id) {
        return new Promise((resolve, reject) =>{
            let cuisine = this.CUISINE.join(',');
            let daysOpen = this.DAYSOPEN.join(',');
            let datos = {
                NAME: this.NAME,
                DESCRIPTION: this.DESCRIPTION,
                DAYSOPEN: daysOpen,
                PHONE: this.PHONE,
                IMAGE: this.IMAGE,
                CUISINE: cuisine,
                CREATORID: id,
                STARS: this.STARS,
                LAT: this.LAT,
                LNG: this.LNG,
                ADDRESS: this.ADDRESS
            };
            conexion.query("INSERT INTO RESTAURANT SET ? ",
            [datos],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else {
                    datos.CUISINE = datos.CUISINE.split(',');
                    datos.DAYSOPEN = datos.DAYSOPEN.split(',');

                    return resolve(datos);
                }
            })
        })
    }

    putRestaurant(restId, id) {
        return new Promise((resolve, reject) =>{
            let cuisine = this.CUISINE.join(',');
            let daysOpen = this.DAYSOPEN.join(',');
            let datos = {
                NAME: this.NAME,
                DESCRIPTION: this.DESCRIPTION,
                DAYSOPEN: daysOpen,
                PHONE: this.PHONE,
                IMAGE: this.IMAGE,
                CUISINE: cuisine,
                CREATORID: id,
                STARS: this.STARS,
                LAT: this.LAT,
                LNG: this.LNG,
                ADDRESS: this.ADDRESS
            };
            conexion.query("UPDATE RESTAURANT SET ? WHERE ID = ? AND CREATORID = ? ",
            [datos, restId, id],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else {
                    datos.CUISINE = datos.CUISINE.split(',');
                    datos.DAYSOPEN = datos.DAYSOPEN.split(',');
                    
                    return resolve(datos);
                }
            })
        })
    }
}