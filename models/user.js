const conexion = require('./../config/bdconfig');
const Token = require('../token/token');
const md5 = require('md5');

module.exports = class User {
    constructor(userJSON) {
        this.ID = userJSON.id || -1;
        this.NAME = userJSON.name || '';
        this.PASSWORD = userJSON.password || '';
        this.AVATAR = userJSON.avatar || '';
        this.EMAIL = userJSON.email || '';
        this.LAT = userJSON.lat || 0;
        this.LNG = userJSON.lng || 0;
    }

    static validarUsuario(usuarioJSON) {
        return new Promise((resolve, reject) =>{
            let passwordEncrypted = md5(usuarioJSON.password + '');
            conexion.query("SELECT ID, EMAIL, PASSWORD FROM USER WHERE EMAIL = ? AND PASSWORD = ?",
            [usuarioJSON.email, passwordEncrypted],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(Token.generarToken(resultado[0].ID));
            })
        })
    }

    static validarUsuarioOAuth(usuarioJSON) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT ID, EMAIL, PASSWORD FROM USER WHERE EMAIL = ?",
            [usuarioJSON.EMAIL],
            (error, resultado, campos) =>{

                if (error || resultado.length == 0) return reject(error);
                else return resolve(Token.generarToken(resultado[0].ID));
            })
        })
    }

    static getUser(id) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM USER WHERE ID = ? ",
            [id],
            (error, resultado, campos) =>{

                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    nuevoUsuario() {
        return new Promise((resolve, reject) =>{
            let passwordEncrypted = md5(this.PASSWORD + '');
            let datos = {
                NAME: this.NAME,
                EMAIL: this.EMAIL,
                PASSWORD: passwordEncrypted,
                LAT: this.LAT,
                LNG: this.LNG,
                AVATAR: this.AVATAR
            };
            conexion.query("INSERT INTO USER SET ? ",
            datos, (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    updateUsuario(id) {
        return new Promise((resolve, reject) =>{
            let passwordEncrypted = md5(this.PASSWORD + '');
            let datos = {
                NAME: this.NAME,
                EMAIL: this.EMAIL,
                PASSWORD: passwordEncrypted,
                LAT: this.LAT,
                LNG: this.LNG,
                AVATAR: this.AVATAR
            };
            conexion.query("UPDATE USER SET ? WHERE ID = ? ",
            [datos, id],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }
}