const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");
const { Strategy, ExtractJwt } = require("passport-jwt");
const usuarios = require("./routes/users");
const restaurantes = require('./routes/restaurants');
const auth = require('./routes/auth');
const constant = require('./constants/constants');

const secreto = constant.secreto;

let app = express();
app.use(passport.initialize());
app.use(bodyParser.json());

passport.use(
  new Strategy(
    {
      secretOrKey: secreto,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    (payload, done) => {;
      if (payload.id) {
        return done(null, { id: payload.id });
      } else {
        return done(new Error("Usuario incorrecto"), null);
      }
    }
  )
);

app.use('/auth', auth);
app.use('/restaurants', restaurantes);
app.use('/users', usuarios);

app.listen(8888);
